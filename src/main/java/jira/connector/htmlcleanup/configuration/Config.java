package jira.connector.htmlcleanup.configuration;

import java.net.URI;

public class Config {

	private URI attlassian_url;
	private String attlassian_username;
	private String attlassian_password;
	private String path;

	public Config(URI attlassian_url, String attlassian_username, String attlassian_password, String path){
		this.attlassian_url = attlassian_url;
		this.attlassian_username = attlassian_username;
		this.attlassian_password = attlassian_password;
		this.path = path;
	}

	public URI getAttlassian_url () {
		return attlassian_url;
	}

	public String getAttlassian_username () {
		return attlassian_username;
	}

	public String getAttlassian_password () {
		return attlassian_password;
	}

	public String getPath() {
		return path;
	}
}
