package jira.connector.htmlcleanup.configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class FileHandler {

	private static final String CONFIG_PATH =  System.getenv("JIRA_HEAT_CONNECTOR_CONFIG_PATH");

	public Config findFile () throws FileNotFoundException, URISyntaxException {
		System.out.println(CONFIG_PATH);
		return findJson(new File(CONFIG_PATH));
	}

	private Config findJson (File configFile) throws FileNotFoundException, URISyntaxException {
		return extractConfigData(new JsonParser().parse(new FileReader(configFile)).getAsJsonObject());
	}

	private Config extractConfigData (JsonObject config) throws URISyntaxException {

		return new Config(new URI(config.get("attlassian_url").getAsString()),
				config.get("attlassian_username").getAsString(), config.get("attlassian_password").getAsString(), CONFIG_PATH);
	}

	public FileHandler () {
	}
}
