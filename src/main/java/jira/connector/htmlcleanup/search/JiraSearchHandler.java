package jira.connector.htmlcleanup.search;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.SearchRestClient;
import com.atlassian.jira.rest.client.api.domain.SearchResult;

import io.atlassian.util.concurrent.Promise;

public class JiraSearchHandler {

	private static SearchRestClient searchClient;

	public JiraSearchHandler (JiraRestClient jiraRestClient) {
		searchClient = jiraRestClient.getSearchClient();
	}

	public SearchResult searchSingleIssues (String issuekey) {

		Promise<SearchResult> searchJql = searchClient.searchJql(issuekey);
		return searchJql.claim();
	}

}
