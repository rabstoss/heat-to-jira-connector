package jira.connector.htmlcleanup.converter;

import java.util.Collections;

import org.jetbrains.annotations.NotNull;

import com.vladsch.flexmark.html2md.converter.FlexmarkHtmlConverter;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.data.MutableDataHolder;
import com.vladsch.flexmark.util.data.MutableDataSet;
import com.vladsch.flexmark.html2md.converter.FlexmarkHtmlConverter.Builder;

public class HTMLConverter {

	public HTMLConverter () {
	}

	public static String toMarkdown (String htmlString) {

		MutableDataSet options = new MutableDataSet().set(Parser.EXTENSIONS,
				Collections.singletonList(HtmlConverterTextExtension.create()));
		Builder builder = FlexmarkHtmlConverter.builder(options);
		return builder.build().convert(htmlString);
	}

	static class HtmlConverterTextExtension implements FlexmarkHtmlConverter.HtmlConverterExtension {

		public static HtmlConverterTextExtension create () {
			return new HtmlConverterTextExtension();
		}

		@Override
		public void rendererOptions (@NotNull MutableDataHolder options) {

		}

		@Override
		public void extend (@NotNull Builder builder) {

		}
	}
}
