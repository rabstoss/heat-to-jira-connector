package jira.connector.htmlcleanup.cleanup;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;

public class JiraIssue {

	private String jiradescription;
	private List<String> embeddedFiles;
	private List<byte[]> jiraAttachments;

	public JiraIssue (String markdowndescription) {
		embeddedFiles = new ArrayList<>();
		jiraAttachments = new ArrayList<>();

		countEmbeddedFiles(markdowndescription);
		removeFileFromDescription(markdowndescription);
	}

	public String getJiradescription () {
		return jiradescription;
	}

	public List<byte[]> getJiraAttachments () {
		return jiraAttachments;
	}

	private void countEmbeddedFiles (String markdowndescription) {

		Pattern pattern = Pattern.compile("!\\[.*?\\]*\\(.*?\\)");
		Matcher matcher = pattern.matcher(markdowndescription);
		while (matcher.find()) {
			embeddedFiles.add(matcher.group());
		}
	}

	private void removeFileFromDescription (String markdowndescrition) {
		for (int i = 0; i != embeddedFiles.size(); i++) {

			markdowndescrition = markdowndescrition.replace(embeddedFiles.get(i), "!Attached_File_" + (i + 1)+".png|thumbnail!");
			embeddedFiles.set(i, embeddedFiles.get(i).replaceAll("!\\[.*?\\]*\\(", "![]("));
			jiraAttachments.add(convertToPureByteArray(embeddedFiles.get(i)));
		}
		jiradescription = markdowndescrition;
	}

	private byte[] convertToPureByteArray(String input){

		String  base64input = input.substring(input.indexOf(",")+1,input.lastIndexOf(")"));

		return Base64.decodeBase64(base64input);
	}

}
