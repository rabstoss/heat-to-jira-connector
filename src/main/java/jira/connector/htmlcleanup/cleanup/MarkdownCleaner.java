package jira.connector.htmlcleanup.cleanup;

public class MarkdownCleaner {

	public static String cleanMarkdownString (String input) {
		input = removeOutlookFragments(input);
		input = removeUncleanFileInputs(input);
		input = removeForgottenInput(input);
		return input;

	}

	private static String removeOutlookFragments (String input) {
		if (input.contains(
				"<\\\\!--\\\\\\[if gte mso 9\\]\\> \\<\\\\!\\\\\\[endif\\]--\\>\\<\\\\!--\\\\\\[if gte mso 9\\]\\> \\<\\\\!\\\\\\[endif\\]--\\>\\<\\\\!--\\\\\\[if \\\\!mso\\]\\>\\<\\\\!\\\\\\[endif\\]--\\>")) {

		} else {
			input = input.replace(
					"<\\\\!--\\\\\\[if gte mso 9\\]\\> \\<\\\\!\\\\\\[endif\\]--\\>\\<\\\\!--\\\\\\[if gte mso 9\\]\\> \\<\\\\!\\\\\\[endif\\]--\\>"
					, "");
			input = input.replace(
					"\\<\\\\!--\\\\\\[if \\\\!mso\\]\\>\\<\\\\!\\\\\\[endif\\]--\\>"
					, "");
		}

		return input;
	}

	private static String removeUncleanFileInputs (String input) {

		input = input.replace("![\\\\\"Window\\\\\"](\\\\\"data:image/png;base64,", "![](data:image/png;base64,");
		input = input.replace("=\\\\\")", "=)");
		return input;
	}

	private static String removeForgottenInput (String input) {
		input = input.replace("<br />", "");
		return input;
	}
}
