package jira.connector.htmlcleanup.logging;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.util.HashMap;

import org.joda.time.LocalDate;

import com.atlassian.jira.rest.client.api.domain.Issue;

public class Logger {

	private PrintWriter pw;

	public Logger (String configPath) throws IOException {
		configPath = configPath.replace("JiraConnectionConfig.json", "jira_heat_connector_log.csv");
		prepareLogger(configPath);
	}

	public void logIssue (Issue issue) {
		HashMap<String, String> issueMap = prepareContainer(issue).getIssueMap();
			pw.write(issueMap.get("date") + "," + issueMap.get("time") + "," + issueMap.get("issuekey") + "," + issueMap.get("summary") + "," + "Reason: Updated issue based on label: " + issue.getLabels());
			pw.write("\r\n");
	}

	public void logProblem (Issue issue) {
		HashMap<String, String> issueMap = prepareContainer(issue).getIssueMap();
		pw.write(issueMap.get("date") + "," + issueMap.get("time") + "," + issueMap.get("issuekey") + "," + issueMap.get("summary") + "," + "Reason: Error when updating issue based on label: " + issue.getLabels());
		pw.write("\r\n");
	}

	public void endLogging () {
		pw.close();
	}

	private void prepareLogger (String configPath) throws IOException {
		FileWriter fw = new FileWriter(configPath,true);
		pw = new PrintWriter(fw, true);
	}

	private IssueContainer prepareContainer(Issue issue){
		return IssueContainer
				.builder()
				.date(LocalDate.now().toString())
				.time(LocalTime.now().toString())
				.issuekey(issue.getKey())
				.summary(issue.getSummary())
				.done();
	}
}
