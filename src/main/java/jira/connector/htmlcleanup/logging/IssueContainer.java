package jira.connector.htmlcleanup.logging;

import java.util.HashMap;

public class IssueContainer {

	private HashMap<String, String> issue = new HashMap<>();

	public static IssueContainer builder () {
		return new IssueContainer();
	}

	public IssueContainer date (String date) {
		issue.put("date", date);
		return this;
	}

	public IssueContainer time (String time) {
		issue.put("time", time);
		return this;
	}

	public IssueContainer issuekey (String issuekey) {
		issue.put("issuekey", issuekey);
		return this;
	}

	public IssueContainer summary(String summary){
		issue.put("summary", summary);
		return this;
	}

	public IssueContainer done(){return this;}

	public HashMap<String, String> getIssueMap(){
		return issue;
	}

	private IssueContainer () {
	}

}
