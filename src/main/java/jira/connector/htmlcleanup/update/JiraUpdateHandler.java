package jira.connector.htmlcleanup.update;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.atlassian.jira.rest.client.api.domain.input.AttachmentInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;

import jira.connector.htmlcleanup.cleanup.JiraIssue;
import jira.connector.htmlcleanup.cleanup.MarkdownCleaner;
import jira.connector.htmlcleanup.converter.HTMLConverter;
import jira.connector.htmlcleanup.logging.Logger;

public class JiraUpdateHandler {

	private static IssueRestClient issueClient;
	private static Logger logger;

	public JiraUpdateHandler (JiraRestClient jiraRestClient, Logger logger) {
		issueClient = jiraRestClient.getIssueClient();
		this.logger = logger;
	}

	public void initiateUpdate (SearchResult searchResult) {

		searchResult.getIssues().forEach(issue -> {
			try {
				convertAndUpdateIssue(issue);
			} catch (IOException e) {
				logger.logProblem(issue);
				e.printStackTrace();
			}
			logger.logIssue(issue);
		});

	}

	private static void convertAndUpdateIssue (Issue issue) throws IOException {
		update(issue, new JiraIssue(cleanDescription(issue)));
	}

	private static String cleanDescription(Issue issue){
		String markdowndescription;
		System.out.println(issue.getDescription());
		if(issue.getDescription() != null){
			markdowndescription = MarkdownCleaner
					.cleanMarkdownString(HTMLConverter.toMarkdown(issue.getDescription()));
		}else{
			markdowndescription = "";
		}
		return markdowndescription;
	}

	private static void update (Issue issue, JiraIssue jiraIssue) throws IOException {
		IssueInput input = new IssueInputBuilder().
				setDescription(jiraIssue.getJiradescription()).setSummary(issue.getSummary()).
				setFieldValue("labels", Arrays.asList("HTML2MD-auto-converted")).build();

		issueClient.updateIssue(issue.getKey(), input).claim();

		for (int i = 0; i != jiraIssue.getJiraAttachments().size(); i++) {

			{
				AttachmentInput at = new AttachmentInput("Attached_File_" + (i + 1) + ".png",
						new ByteArrayInputStream(jiraIssue.getJiraAttachments().get(i)));

				issueClient.addAttachments(issue.getAttachmentsUri(), at).claim();
			}

		}
	}
}