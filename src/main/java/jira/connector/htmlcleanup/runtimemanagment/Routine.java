package jira.connector.htmlcleanup.runtimemanagment;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

import jira.connector.htmlcleanup.JiraConnector;

public class Routine implements Job {

	@Override
	public void execute (JobExecutionContext context){
		try {
			JiraConnector.start().
					prepareConfig().
					prepareLogger().
					establishConnectionToJira().
					initiateJiraSearchHandler().
					searchSingleIssueInJira().
					initiateJiraUpdateHandler().
					updateSearchedIssuesInJira().
					done();
		} catch (URISyntaxException | FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
