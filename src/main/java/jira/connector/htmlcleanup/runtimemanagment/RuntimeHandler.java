package jira.connector.htmlcleanup.runtimemanagment;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class RuntimeHandler {

	public RuntimeHandler () throws SchedulerException {
		createScheduler();
	}

	private void createScheduler () throws SchedulerException {
		SchedulerFactory schedulerFactory = new StdSchedulerFactory();
		Scheduler scheduler = schedulerFactory.getScheduler();
		prepareScheduler(scheduler);
	}

	private void prepareScheduler (Scheduler scheduler) throws SchedulerException {
		scheduler.scheduleJob(prepareRoutine(), prepareTrigger());
		startRoutine(scheduler);
	}

	private JobDetail prepareRoutine () {
		return JobBuilder.newJob(Routine.class)
				.withIdentity("routine", "jiraconnector")
				.build();
	}

	private Trigger prepareTrigger () {
		return TriggerBuilder.newTrigger()
				.withIdentity("routineintervall", "jiraconnector")
				.startNow()
				.withSchedule(SimpleScheduleBuilder.simpleSchedule()
						.withIntervalInSeconds(300)
						.repeatForever())
				.build();
	}

	private void startRoutine (Scheduler scheduler) throws SchedulerException {
		scheduler.start();
	}
}
