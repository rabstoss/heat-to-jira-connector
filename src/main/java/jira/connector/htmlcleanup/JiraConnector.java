package jira.connector.htmlcleanup;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.SearchResult;

import jira.connector.htmlcleanup.configuration.Config;
import jira.connector.htmlcleanup.configuration.FileHandler;
import jira.connector.htmlcleanup.connection.JiraConnectionHandler;
import jira.connector.htmlcleanup.logging.Logger;
import jira.connector.htmlcleanup.search.JiraSearchHandler;
import jira.connector.htmlcleanup.update.JiraUpdateHandler;

public class JiraConnector {

	private JiraRestClient jiraConnection;
	private JiraSearchHandler jiraSearchHandler;
	private JiraUpdateHandler jiraUpdateHandler;

	private SearchResult searchresult;
	private Logger logger;

	//Issuekey example: "issuekey = SAP-2720"
	//Labels example: "labels = Heat-Import"
	private static final String SEARCHPROFIL = "labels = Heat-Import";
	private Config config;

	public static JiraConnector start () {
		return new JiraConnector();
	}

	public JiraConnector prepareConfig () throws FileNotFoundException, URISyntaxException {
		config = new FileHandler().findFile();
		return this;
	}

	public JiraConnector prepareLogger() throws IOException {
		logger = new Logger(config.getPath());
		return this;
	}

	public JiraConnector establishConnectionToJira () {
		JiraConnectionHandler connectionHandler = new JiraConnectionHandler(config);
		jiraConnection = connectionHandler.createNewJiraRestClient();
		return this;
	}

	public JiraConnector initiateJiraSearchHandler () {
		jiraSearchHandler = new JiraSearchHandler(jiraConnection);
		return this;
	}

	public JiraConnector searchSingleIssueInJira () {
		searchresult = jiraSearchHandler.searchSingleIssues(SEARCHPROFIL);
		return this;
	}

	public JiraConnector initiateJiraUpdateHandler () {
		jiraUpdateHandler = new JiraUpdateHandler(jiraConnection, logger);
		return this;
	}

	public JiraConnector updateSearchedIssuesInJira () {
		jiraUpdateHandler.initiateUpdate(searchresult);
		return this;
	}

	public void done () throws IOException {
		jiraConnection.close();
		logger.endLogging();
	}

	private JiraConnector () {
	}
}
