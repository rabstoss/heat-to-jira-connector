package jira.connector.htmlcleanup.connection;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;

import jira.connector.htmlcleanup.configuration.Config;

public class JiraConnectionHandler {

	Config config;

	public JiraConnectionHandler (Config config) {
		this.config = config;
	}

	public JiraRestClient createNewJiraRestClient () {
		JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
		return factory.createWithBasicHttpAuthentication(config.getAttlassian_url(), config.getAttlassian_username(), config.getAttlassian_password());
	}


}
