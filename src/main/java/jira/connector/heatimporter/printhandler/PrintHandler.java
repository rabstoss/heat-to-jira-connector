package jira.connector.heatimporter.printhandler;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.atlassian.jira.rest.client.api.domain.input.IssueInput;

import jira.connector.heatimporter.jsonhandling.gson.Task;

public class PrintHandler {

	private static PrintWriter pw;

	public static boolean givePrintWriterDirection (String configPath) {
		try {
			configPath = configPath.replace("JiraConnectionConfig.json", "jira_heat_migration_log.csv");
			FileWriter fw = new FileWriter(configPath, true);
			pw = new PrintWriter(fw, true);
			return true;
		} catch (Exception e) {
			Logger.getLogger(Logger.class.getName()).log(Level.SEVERE,"An error occurred while the print-writer was created");
			return false;
		}
	}

	public static void closeLoggingFile () {
		pw.close();
	}

	public static void printGenericHttpRequestCreationError (Exception e) {
		logInput("An error occurred while a HTTP Request was created.\nErrorMessage: " + e);
	}

	public static void printGenericJSONConversionError (Exception e) {
		logInput("An error occurred while a JSon was created.\nErrorMessage: " + e);
	}

	public static void printGenericHTTPEntityConversionError (Exception e) {
		logInput("An error occurred while a HTTP Entity was created.\nErrorMessage: " + e);
	}

	public static void printGenericIssueInputIsNull () {
		logInput("IssueInput is null.");
	}

	public static void printGenericProgressionSySo (String stepInfo) {
		logInput("Progression information: " + stepInfo);
	}

	public static void printTotalErrorConnection () {
		logInput(
				"Total Error: Application wasn't able to create the connection handling unit an and was closed!");
	}

	public static void printHTTPRequestError (String typ, Exception e) {
		logInput("The request of " + typ + "wasn't successful.\nErrorMessage:" + e);
	}

	public static void printPartMigrationError (String migrationPart, Exception e) {
		logInput(
				"An error occurred while migrating " + migrationPart + ".\nThe whole prat of the migration was stopped.\nErrorMessage: " + e);
	}

	public static void printConnectionError (String targetSystem, Exception e) {
		logInput(
				"An error occurred while connection to target system '" + targetSystem + "'.\nNo Connection was established!\nErrorMessage:" + e);
	}

	public static void printCountRequestError (String typ, Exception e) {
		logInput("Error when fetching the int count of " + typ + ".\nErrorMessage: " + e);
	}

	public static void printSkipRequestError (String typ, int skip, Exception e) {
		logInput(
				"An error occurred when requesting a row of " + typ + " objects from Heat with a skip of " + skip + ".\nErrorMessage: " + e);
	}

	public static void printSkipAndTopRequestError (String typ, int skip, int top, Exception e) {
		logInput(
				"An error occurred when requesting a row of " + typ + " objects from Heat with a skip of " + skip + " and a top of " + top + ".\nErrorMessage: " + e);
	}

	public static void printTokenCreationError (Exception e) {
		logInput("An error occurred while a connection token was created.\nErrorMessage: " + e);
	}

	public static void printMissingParentForTask (Task to) {
		logInput("No parent was found for the task " + to.getRecId());
	}

	public static void printJiraIssueCreationError (IssueInput i, Exception e) {
		logInput("An error occurred while Issue Object\nIssue Error: " + i.getField(
				"customfield_10538") + "\ncreation was requested.\nObject wasn't created!\nErrorMessage: " + e);
	}

	public static void printMigrationTimeStamp (String typ, int pos, int max) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		Logger.getLogger(Logger.class.getName()).log(Level.INFO,"Migration process " + typ + "Count: " + pos + "/" + max + ";TimeStamp: " + timestamp);
	}

	public static void printJiraObjectCreationError(String typ, Exception e){
		logInput("An error occurred while a json was converted into a jira object typ " + typ + "\nErrorMessage: " + e);
	}

	private static void logInput (String input) {
		pw.write(input);
		pw.write("\r\n");
	}

	private PrintHandler(){};
}
