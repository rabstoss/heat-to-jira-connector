package jira.connector.heatimporter.configuration;

import com.google.gson.annotations.SerializedName;

public class SerialConfig {
	@SerializedName ("attlassian_url")
	private String atlassianUrl;

	@SerializedName("attlassian_username")
	private String atlassianUsername;

	@SerializedName("attlassian_password")
	private String atlassianPassword;

	public String getAtlassianUrl () {
		return atlassianUrl;
	}

	public String getAtlassianUsername () {
		return atlassianUsername;
	}

	public String getAtlassianPassword () {
		return atlassianPassword;
	}
}
