package jira.connector.heatimporter.configuration;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;

public class ConfigHandler {

	private static final String CONFIG_PATH = System.getenv("JIRA_HEAT_CONNECTOR_CONFIG_PATH");

	public Config findFile () throws URISyntaxException, IOException {
		String content = readFile(CONFIG_PATH, StandardCharsets.UTF_8);
		SerialConfig serialConfig = new Gson().fromJson(content, SerialConfig.class);
		return new Config(serialConfig);
	}

	private static String readFile (String path, Charset encoding)
			throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
}
