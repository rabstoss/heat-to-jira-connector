package jira.connector.heatimporter.configuration;

import java.net.URI;
import java.net.URISyntaxException;

public class Config {

	private URI atlassianUrl;
	private String atlassianUsername;
	private String atlassianPassword;

	public Config (SerialConfig serialConfig) throws URISyntaxException {

		atlassianUrl = new URI(serialConfig.getAtlassianUrl());
		atlassianUsername = serialConfig.getAtlassianUsername();
		atlassianPassword = serialConfig.getAtlassianPassword();
	}

	public URI getAtlassianUrl () {
		return atlassianUrl;
	}

	public String getAtlassianUsername () {
		return atlassianUsername;
	}

	public String getAtlassianPassword () {
		return atlassianPassword;
	}

}
