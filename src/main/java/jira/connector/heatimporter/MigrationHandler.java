package jira.connector.heatimporter;

import java.util.HashMap;

import jira.connector.heatimporter.migrationhandling.AttachmentMigration;
import jira.connector.heatimporter.migrationhandling.IncidentMigration;
import jira.connector.heatimporter.migrationhandling.EMailMigration;
import jira.connector.heatimporter.migrationhandling.NoteMigration;
import jira.connector.heatimporter.migrationhandling.ServiceRequestMigration;
import jira.connector.heatimporter.migrationhandling.TaskMigration;
import jira.connector.heatimporter.connection.ConnectionManager;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class MigrationHandler {

	private static ConnectionManager connectionManager;

	public static void start () {
		connectionManager = new ConnectionManager();
		if (connectionManager.validateConnections() && PrintHandler
				.givePrintWriterDirection(System.getenv("JIRA_HEAT_CONNECTOR_CONFIG_PATH"))) {
			migrationProcess();
		} else {
			PrintHandler.printTotalErrorConnection();
		}
	}

	private static void migrationProcess () {
		HashMap<String, String> recIdToJiraMapping = new HashMap<>();
		recIdToJiraMapping.putAll(new IncidentMigration(connectionManager).startMigration());
		recIdToJiraMapping.putAll(new ServiceRequestMigration(connectionManager).startMigration());
		recIdToJiraMapping.putAll(new TaskMigration(connectionManager).startMigration(recIdToJiraMapping));
		recIdToJiraMapping.putAll(new EMailMigration(connectionManager).startMigration(recIdToJiraMapping));
		recIdToJiraMapping.putAll(new NoteMigration(connectionManager).startMigration(recIdToJiraMapping));
		new AttachmentMigration(connectionManager).startMigration(recIdToJiraMapping);
		PrintHandler.closeLoggingFile();
	}

	private MigrationHandler(){}
}