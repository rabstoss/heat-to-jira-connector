package jira.connector.heatimporter.connection.heat;

import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONObject;

import jira.connector.heatimporter.printhandler.PrintHandler;

public class HeatConnectionHandler {

	private final String heatConnectionString = "http://PIL-HEAT-SM.piller.de";
	private final String heatTokenString = "/HEAT/api/rest/authentication/login";

	private final String heatIncident = "/HEAT/api/odata/businessobject/incidents";
	private final String heatServiceRequests = "/HEAT/api/odata/businessobject/servicereqs";
	private final String heatTasks = "/HEAT/api/odata/businessobject/tasks";
	private final String heatJournalNotes = "/HEAT/api/odata/businessobject/Journal__Notess";
	private final String heatJournalEMails = "/HEAT/api/odata/businessobject/Journal__Emails";
	private final String heatAttachmentsOnID = "/HEAT/api/rest/Attachment?ID=";
	private final String heatAttachments = "/HEAT/api/odata/businessobject/attachments";


	private HttpClient httpclient;
	private String token;

	public HeatConnectionHandler (){
		refreshClient();
	}

	public String requestIncidentsFromHeat (int skip) {
		try {
			HttpGet httpGet = createHttpGet(
					heatIncident + createNewSkipAndTopPartWithFilter(skip,
							"$orderby=recId&$filter=Status+eq+%27Active%27"));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipRequestError("Incident", skip, e);
			return "";
		}
	}

	public String requestIncidentsFromHeat (int skip, int top) {
		try {
			HttpGet httpGet = createHttpGet(
					heatIncident + createNewSkipAndTopPartWithFilter(skip, top,
							"$orderby=recId&$filter=Status+eq+%27Active%27"));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipAndTopRequestError("Incident", skip, top, e);
			return "";
		}
	}

	public int requestCountOfIncidents () {
		try {
			HttpGet httpGet = createHttpGet(heatIncident + "?@odata.count&$filter=Status+eq+%27Active%27");
			HttpResponse response = httpclient.execute(httpGet);
			return extractOdataCount(handleResponseEntity(response.getEntity()));
		} catch (Exception e) {
			PrintHandler.printCountRequestError("Incident", e);
			return 0;
		}
	}

	public String requestServiceRequestsFromHeat (int skip) {
		try {
			HttpGet httpGet = createHttpGet(
					heatServiceRequests + createNewSkipAndTopPartWithFilter(skip,
							"$orderby=recId&$filter=Status+eq+%27Active%27"));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipRequestError("ServiceRequest", skip, e);
			return "";
		}
	}

	public String requestServiceRequestsFromHeat (int skip, int top) {
		try {
			HttpGet httpGet = createHttpGet(
					heatServiceRequests + createNewSkipAndTopPartWithFilter(skip, top,
							"$orderby=recId&$filter=Status+eq+%27Active%27"));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipAndTopRequestError("Service Request", skip, top, e);
			return "";
		}
	}

	public int requestCountOfServiceRequest () {
		try {
			HttpGet httpGet = createHttpGet(heatServiceRequests + "?@odata.count&$filter=Status+eq+%27Active%27");
			HttpResponse response = httpclient.execute(httpGet);
			return extractOdataCount(handleResponseEntity(response.getEntity()));
		} catch (Exception e) {
			PrintHandler.printCountRequestError("Service Request", e);
			return 0;
		}
	}

	public String requestTasksFromHeat (int skip) {
		try {
			HttpGet httpGet = createHttpGet(
					heatTasks + createNewSkipAndTopPartWithFilter(skip, "$orderby=recId"));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipRequestError("Task", skip, e);
			return "";
		}
	}

	public String requestTasksFromHeat (int skip, int top) {
		try {
			HttpGet httpGet = createHttpGet(
					heatTasks + createNewSkipAndTopPartWithFilter(skip, top, "$orderby=recId"));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipAndTopRequestError("Task", skip, top, e);
			return "";
		}
	}

	public int requestCountOfTask () {
		try {
			HttpGet httpGet = createHttpGet(heatTasks + "?@odata.count&$orderby=recId");
			HttpResponse response = httpclient.execute(httpGet);
			return extractOdataCount(handleResponseEntity(response.getEntity()));
		} catch (Exception e) {
			PrintHandler.printCountRequestError("Task", e);
			return 0;
		}
	}

	public String requestJournalNotesFromHeat (int skip) {
		try {
			HttpGet httpGet = createHttpGet(heatJournalNotes + createNewSkipAndTopPart(skip));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipRequestError("JournalNote", skip, e);
			return "";
		}
	}

	public String requestJournalNotesFromHeat (int skip, int top) {
		try {
			HttpGet httpGet = createHttpGet(heatJournalNotes + createNewSkipAndTopPart(skip, top));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipAndTopRequestError("Journal Note", skip, top, e);
			return "";
		}

	}

	public int requestCountOfJournalNotes () {
		try {
			HttpGet httpGet = createHttpGet(heatJournalNotes + "?@odata.count");
			HttpResponse response = httpclient.execute(httpGet);
			return extractOdataCount(handleResponseEntity(response.getEntity()));
		} catch (Exception e) {
			PrintHandler.printCountRequestError("Journal Note", e);
			return 0;
		}
	}

	public String requestJournalEmailsFromHeat (int skip) {
		try {
			HttpGet httpGet = createHttpGet(heatJournalEMails + createNewSkipAndTopPart(skip));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipRequestError("EMail Journal", skip, e);
			return "";
		}

	}

	public String requestJournalEmailsFromHeat (int skip, int top) {
		try {
			HttpGet httpGet = createHttpGet(heatJournalEMails + createNewSkipAndTopPart(skip, top));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipAndTopRequestError("EMail Journal", skip, top, e);
			return "";
		}
	}

	public int requestCountOfJournalEmails () {
		try {
			HttpGet httpGet = createHttpGet(heatJournalEMails + "?@odata.count");
			HttpResponse response = httpclient.execute(httpGet);
			return extractOdataCount(handleResponseEntity(response.getEntity()));
		} catch (Exception e) {
			PrintHandler.printCountRequestError("EMail Journal", e);
			return 0;
		}
	}

	public String requestAttachmentsFromHeat (int skip) {
		try {
			HttpGet httpGet = createHttpGet(heatAttachments + createNewSkipAndTopPart(skip));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipRequestError("Heat Attachments", skip, e);
			return "";
		}

	}

	public String requestAttachmentsFromHeat (int skip, int top) {
		try {
			HttpGet httpGet = createHttpGet(heatAttachments + createNewSkipAndTopPart(skip, top));
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printSkipAndTopRequestError("Heat Attachments", skip, top, e);
			return "";
		}
	}

	public int requestCountOfAttachments() {
		try {
			HttpGet httpGet = createHttpGet(heatAttachments + "?@odata.count");
			HttpResponse response = httpclient.execute(httpGet);
			return extractOdataCount(handleResponseEntity(response.getEntity()));
		} catch (Exception e) {
			PrintHandler.printCountRequestError("Heat Attachments", e);
			return 0;
		}
	}

	public String requestAttachmentsBasedOnID (String id) {
		try {
			HttpGet httpGet = createHttpGet(heatAttachmentsOnID + id);
			HttpResponse response = httpclient.execute(httpGet);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printHTTPRequestError("Attachments", e);
			return "";
		}
	}

	private String requestTokenFromHeat () {
		try {
			HttpPost httpPost = createTokenHttpPost();
			HttpResponse response = httpclient.execute(httpPost);
			return handleResponseEntity(response.getEntity());
		} catch (Exception e) {
			PrintHandler.printHTTPRequestError("GET HeatConnectionToken", e);
			return "";
		}
	}

	private HttpPost createTokenHttpPost () {
		try {
			String toUri = heatConnectionString + heatTokenString;
			HttpPost httpPost = new HttpPost(new URI(toUri));

			JSONObject bodyJson = new JSONObject();
			bodyJson.put("tenant", "PIL-HEAT-SM.piller.de");
			bodyJson.put("username", "HEATAdmin");
			bodyJson.put("password", "P!llerSM2016");
			bodyJson.put("role", "Admin");

			HttpEntity body = new ByteArrayEntity(bodyJson.toString().getBytes("UTF-8"));

			httpPost.addHeader("content-type", "application/json");

			httpPost.setEntity(body);

			return httpPost;
		} catch (Exception e) {
			PrintHandler.printTokenCreationError(e);
			return null;
		}
	}

	private HttpGet createHttpGet (String subURLString) {
		try {
			String toUri = heatConnectionString + subURLString;
			HttpGet httpGet = new HttpGet(new URI(toUri));
			httpGet.addHeader("Authorization", token);
			return httpGet;
		} catch (Exception e) {
			PrintHandler.printGenericHttpRequestCreationError(e);
			return null;
		}
	}

	private String createNewSkipAndTopPart (int skip) {
		if (skip == 0) {
			return "?$top=100&$orderby=recId";
		} else {
			return "?$top=100&$skip=" + skip + "&$orderby=recId";
		}
	}

	private String createNewSkipAndTopPartWithFilter (int skip, String filter) {
		if (skip == 0) {
			return "?$top=100&" + filter;
		} else {
			return "?$top=100&$skip=" + skip + "&" + filter;
		}
	}

	private String createNewSkipAndTopPart (int skip, int top) {
		if (skip == 0) {
			return "?$top=" + String.valueOf(top) + "&$orderby=recId";
		} else {
			return "?$top=" + String.valueOf(top) + "&$skip=" + String.valueOf(skip) + "&$orderby=recId";
		}
	}

	private String createNewSkipAndTopPartWithFilter (int skip, int top, String filter) {
		if (skip == 0) {
			return "?$top=" + top + "&" + filter;
		} else {
			return "?$top=" + top + "&$skip=" + skip + "&" + filter;
		}
	}

	private int extractOdataCount (String jsonString) {
		try {
			return Integer.valueOf(new JSONObject(jsonString).get("@odata.count").toString());
		} catch (Exception e) {
			PrintHandler.printGenericJSONConversionError(e);
			return 0;
		}
	}

	public void refreshClient(){
		httpclient = HttpClients.createDefault();
		token = requestTokenFromHeat().replace("\"", "");
	}

	private String handleResponseEntity (HttpEntity httpEntity) {
		try {
			return EntityUtils.toString(httpEntity);
		} catch (Exception e) {
			PrintHandler.printGenericHTTPEntityConversionError(e);
			return "";
		}
	}
}