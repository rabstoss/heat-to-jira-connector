package jira.connector.heatimporter.connection.jira;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;

import jira.connector.heatimporter.configuration.Config;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class JiraTaskCreationHandler {

	JiraRestClient jiraRestClient;
	Config config;

	public JiraTaskCreationHandler (Config config) {
		this.config = config;
		startNewClient();
	}

	public String postNewIssueToJira (IssueInput issue) {
		if (issue != null) {
			try {
				refreshClient();
				return jiraRestClient.getIssueClient().createIssue(issue).claim().getKey();
			} catch (Exception e) {
				PrintHandler.printJiraIssueCreationError(issue, e);
				return "";
			}
		} else {
			PrintHandler.printGenericIssueInputIsNull();
			return "";
		}
	}

	public void addJournalsAsComments (String journalContent, String jiraId) {
		try {
			Issue issue = requestIncidentJsonFromJira(jiraId);
			refreshClient();
			jiraRestClient.getIssueClient().addComment(issue.getCommentsUri(), Comment.valueOf(journalContent)).claim();
		} catch (Exception e) {
			PrintHandler.printHTTPRequestError("Journal with id" + jiraId, e);
		}
	}

	private Issue requestIncidentJsonFromJira (String id) {
		try {
			refreshClient();
			return jiraRestClient.getIssueClient().getIssue(id).claim();
		} catch (Exception e) {
			PrintHandler.printHTTPRequestError("IssueIdRequest", e);
			return null;
		}
	}

	public void postAttachmentToJiraIssue (InputStream is, String filename, String jiraId) {
		try {
			refreshClient();
			URI attachmentUri = requestIncidentJsonFromJira(jiraId).getAttachmentsUri();
			jiraRestClient.getIssueClient().addAttachment(attachmentUri,is,filename).claim();
		} catch (Exception e) {
		}
	}

	private void refreshClient () {
		try {
			closeClient();
			startNewClient();
		} catch (Exception e) {
			PrintHandler.printConnectionError("Jira", e);
		}
	}

	private void startNewClient () {
		JiraConnectionHandler jiraConnectionHandler = new JiraConnectionHandler(config);
		jiraRestClient = jiraConnectionHandler.createNewJiraRestClient();
	}

	private void closeClient () throws IOException {
		jiraRestClient.close();
	}
}
