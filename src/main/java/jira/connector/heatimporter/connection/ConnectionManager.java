package jira.connector.heatimporter.connection;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jira.connector.heatimporter.configuration.ConfigHandler;
import jira.connector.heatimporter.connection.heat.HeatConnectionHandler;
import jira.connector.heatimporter.connection.jira.JiraTaskCreationHandler;
import jira.connector.heatimporter.jira.issueinput.IssueInputContainer;
import jira.connector.heatimporter.jira.issueinput.IssueInputFactory;
import jira.connector.heatimporter.jsonhandling.gson.AttachmentIdentifier;
import jira.connector.heatimporter.jsonhandling.gson.EmailJournal;
import jira.connector.heatimporter.jsonhandling.gson.NoteJournal;
import jira.connector.heatimporter.jsonhandling.gson.Task;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class ConnectionManager {

	private HeatConnectionHandler heatConnectionHandler;
	private JiraTaskCreationHandler jiraTaskCreationHandler;

	private boolean heatConnectionEstablished;
	private boolean jiraConnectionEstablished;

	public ConnectionManager () {
		createHeatConnection();
		createJiraConnection();
	}

	public HeatConnectionHandler getHeatConnectionHandler () {
		heatConnectionHandler.refreshClient();
		return heatConnectionHandler;
	}

	public boolean validateConnections () {
		return Boolean.logicalAnd(heatConnectionEstablished, jiraConnectionEstablished);
	}

	public Map<String, String> postIssuesToJira (List<IssueInputContainer> issues) {
		HashMap<String, String> recIdToJiraMapping = new HashMap<>();
		for (int i = 0; i != issues.size(); i++) {
			String responseKey = jiraTaskCreationHandler.postNewIssueToJira(issues.get(i).getIssueInput());
			if (responseKey != null)
				recIdToJiraMapping.put(issues.get(i).getRecId(), responseKey);
		}
		return recIdToJiraMapping;
	}

	public Map<String, String> postTaskToJira (List<Task> taskObjects,
			Map<String, String> recIdToJiraMapping) {
		HashMap<String, String> tempRecIdMapping = new HashMap<>();

		for (int i = 0; i != taskObjects.size(); i++) {
			if (recIdToJiraMapping.containsKey(taskObjects.get(i).getParentLink_RecID())) {
				String responseKey = jiraTaskCreationHandler
						.postNewIssueToJira(IssueInputFactory.build(taskObjects.get(i),
								recIdToJiraMapping.get(taskObjects.get(i).getParentLink_RecID())).getIssueInput());
				if (responseKey != null) {
					tempRecIdMapping.put(taskObjects.get(i).getRecId(), responseKey);
				}
			} else {
				PrintHandler.printMissingParentForTask(taskObjects.get(i));
			}
		}
		return tempRecIdMapping;
	}

	public Map<String, String> postEMailJournalsToJira (List<EmailJournal> journalObjects,
			Map<String, String> recIdToJiraMapping) {
		HashMap<String, String> commentRecIdMapping = new HashMap<>();
		for (EmailJournal journalObject : journalObjects) {
			if (recIdToJiraMapping.containsKey(journalObject.getParentLink_RecID())) {
				jiraTaskCreationHandler.addJournalsAsComments(journalObject.getCommentBody(),
						recIdToJiraMapping.get(journalObject.getParentLink_RecID()));
				commentRecIdMapping
						.put(journalObject.getRecId(), recIdToJiraMapping.get(journalObject.getParentLink_RecID()));
			}
		}
		return commentRecIdMapping;
	}

	public Map<String, String> postNoteJournalsToJira (List<NoteJournal> journalObjects,
			Map<String, String> recIdToJiraMapping) {
		HashMap<String, String> commentRecIdMapping = new HashMap<>();
		for (NoteJournal journalObject : journalObjects) {
			if (recIdToJiraMapping.containsKey(journalObject.getParentLink_RecID())) {
				jiraTaskCreationHandler.addJournalsAsComments(journalObject.getCommentBody(),
						recIdToJiraMapping.get(journalObject.getParentLink_RecID()));
				commentRecIdMapping
						.put(journalObject.getRecId(), recIdToJiraMapping.get(journalObject.getParentLink_RecID()));
			}
		}
		return commentRecIdMapping;
	}

	public void postAttachmentToJira (List<AttachmentIdentifier> tempList, Map<String, String> recIdToJiraMapping) {
		for (AttachmentIdentifier attachmentIdentifier : tempList) {
			try {
				if (recIdToJiraMapping.containsKey(attachmentIdentifier.getParentLinkRecId())) {
					String attachment = heatConnectionHandler
							.requestAttachmentsBasedOnID(attachmentIdentifier.getRecId());
					try {
						InputStream is = new ByteArrayInputStream(attachment.getBytes(Charset.forName("UTF-8")));
						String jiraId = recIdToJiraMapping.get(attachmentIdentifier.getParentLinkRecId());
						jiraTaskCreationHandler
								.postAttachmentToJiraIssue(is, attachmentIdentifier.getAttachname(), jiraId);
					} catch (Exception e) {
						System.out.println(e);
					}
				}
			} catch (Exception e) {

			}
		}
	}

	private void createHeatConnection () {
		try {
			heatConnectionHandler = new HeatConnectionHandler();
			heatConnectionEstablished = true;

		} catch (Exception e) {
			PrintHandler.printConnectionError("HEAT", e);
			heatConnectionEstablished = false;
		}

	}

	private void createJiraConnection () {
		try {
			jiraTaskCreationHandler = new JiraTaskCreationHandler(new ConfigHandler().findFile());
			jiraConnectionEstablished = true;

		} catch (Exception e) {
			PrintHandler.printConnectionError("JIRA", e);
			jiraConnectionEstablished = false;
		}

	}

}

