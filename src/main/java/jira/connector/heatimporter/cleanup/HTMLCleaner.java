package jira.connector.heatimporter.cleanup;

import java.util.Collections;

import org.jetbrains.annotations.NotNull;

import com.vladsch.flexmark.html2md.converter.FlexmarkHtmlConverter;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.data.MutableDataHolder;
import com.vladsch.flexmark.util.data.MutableDataSet;

public class HTMLCleaner {

	public static String clean (String input) {
		try {
			return toMarkdownViaFlexmark(input);
		} catch (Exception e) {
			return input;
		}
	}

	private static String toMarkdownViaFlexmark (String htmlString) throws StackOverflowError {

		MutableDataSet options = new MutableDataSet().set(Parser.EXTENSIONS,
				Collections.singletonList(HtmlConverterTextExtension.create()));
		FlexmarkHtmlConverter.Builder builder = FlexmarkHtmlConverter.builder(options);

		String cleanMark = "";
		for (int i = 0; i <= htmlString.length() - 1; i = i + 10000) {
			if (i + 10000 < htmlString.length() - 1) {
				cleanMark = cleanMark + builder.build().convert(htmlString.substring(i, i + 10000));
			} else {
				cleanMark = cleanMark + builder.build().convert(htmlString.substring(i));
			}
		}
		return cleanMarkdownString(cleanMark);
	}

	private static class HtmlConverterTextExtension implements FlexmarkHtmlConverter.HtmlConverterExtension {

		public static HtmlConverterTextExtension create () {
			return new HtmlConverterTextExtension();
		}

		@Override
		public void rendererOptions (@NotNull MutableDataHolder options) {

		}

		@Override
		public void extend (@NotNull FlexmarkHtmlConverter.Builder builder) {

		}
	}

	private static String cleanMarkdownString (String input) {
		input = removeOutlookFragments(input);
		input = removeUncleanFileInputs(input);
		input = removeForgottenInput(input);
		return input;

	}

	private static String removeOutlookFragments (String input) {
		if (input.contains(
				"<\\\\!--\\\\\\[if gte mso 9\\]\\> \\<\\\\!\\\\\\[endif\\]--\\>\\<\\\\!--\\\\\\[if gte mso 9\\]\\> \\<\\\\!\\\\\\[endif\\]--\\>\\<\\\\!--\\\\\\[if \\\\!mso\\]\\>\\<\\\\!\\\\\\[endif\\]--\\>")) {
			input = input.replace(
					"<\\\\!--\\\\\\[if gte mso 9\\]\\> \\<\\\\!\\\\\\[endif\\]--\\>\\<\\\\!--\\\\\\[if gte mso 9\\]\\> \\<\\\\!\\\\\\[endif\\]--\\>"
					, "");
			input = input.replace(
					"\\<\\\\!--\\\\\\[if \\\\!mso\\]\\>\\<\\\\!\\\\\\[endif\\]--\\>"
					, "");
		}

		return input;
	}

	private static String removeUncleanFileInputs (String input) {

		input = input.replace("![\\\\\"Window\\\\\"](\\\\\"data:image/png;base64,", "![](data:image/png;base64,");
		input = input.replace("=\\\\\")", "=)");
		return input;
	}

	private static String removeForgottenInput (String input) {
		input = input.replace("<br />", "");
		return input;
	}

	private HTMLCleaner () {
	}
}
