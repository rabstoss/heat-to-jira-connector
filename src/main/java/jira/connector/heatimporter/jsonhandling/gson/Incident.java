package jira.connector.heatimporter.jsonhandling.gson;

import com.google.gson.annotations.SerializedName;

public class Incident {

	@SerializedName ("ActualCategory")
	private String actualCategory;

	@SerializedName ("Approver")
	private String approver;

	@SerializedName ("ApprovalStatus")
	private String approvalStatus;

	@SerializedName ("Category")
	private String category;

	@SerializedName ("CauseCode")
	private String causeCode;

	@SerializedName ("CreatedBy")
	private String createdBy;

	@SerializedName ("ClosedBy")
	private String closedBy;

	@SerializedName ("ClosedDateTime")
	private String closedDateTime;

	@SerializedName ("ClosedDuration")
	private String closedDuration;

	@SerializedName ("Email")
	private String email;

	@SerializedName ("Impact")
	private String impact;

	@SerializedName ("IncidentNumber")
	private String incidentNumber;

	@SerializedName ("LastModBy")
	private String lastModBy;

	@SerializedName ("Phone")
	private String phone;

	@SerializedName ("Priority")
	private String priority;

	@SerializedName ("ProfileFullName")
	private String profileFullName;

	@SerializedName ("RecId")
	private String recId;

	@SerializedName ("ResolutionString")
	private String resolutionString;

	@SerializedName ("Status")
	private String status;

	@SerializedName ("Service")
	private String service;

	@SerializedName ("Source")
	private String source;

	@SerializedName ("Subject")
	private String subject;

	@SerializedName ("Symptom")
	private String symptom;

	@SerializedName ("Urgency")
	private String urgency;

	@SerializedName ("Owner")
	private String owner;

	@SerializedName ("OwnerTeam")
	private String ownerTeam;

	@SerializedName ("ResolvedBy")
	private String resolvedBy;

	@SerializedName ("SocialTextHeader")
	private String socialTextHeader;

	@SerializedName ("PBC_Resubmission")
	private String pbc_Resubmission;

	@SerializedName ("PBC_NewsReceived")
	private String pbc_NewsReceived;

	@SerializedName ("PBC_Effort")
	private String pbc_Effort;

	@SerializedName ("PBC_ServiceReqRecIDTemp")
	private String pbc_ServiceReqRecIDTemp;

	@SerializedName ("PBC_JiraDescription")
	private String pbc_JiraDescription;

	@SerializedName ("PBC_JiraSynched")
	private String pbc_JiraSynched;

	public String getActualCategory () {
		return nullProof(actualCategory);
	}

	public String getApprover () {
		return nullProof(approver);
	}

	public String getApprovalStatus () {
		return nullProof(approvalStatus);
	}

	public String getCategory () {
		return nullProof(category);
	}

	public String getCauseCode () {
		return nullProof( causeCode);
	}

	public String getCreatedBy () {
		return nullProof( createdBy);
	}

	public String getClosedBy () {
		return nullProof( closedBy);
	}

	public String getClosedDateTime () {
		return nullProof( closedDateTime);
	}

	public String getClosedDuration () {
		return nullProof( closedDuration);
	}

	public String getEmail () {
		return nullProof( email);
	}

	public String getImpact () {
		return nullProof( impact);
	}

	public String getIncidentNumber () {
		return nullProof( incidentNumber);
	}

	public String getLastModBy () {
		return nullProof( lastModBy);
	}

	public String getPhone () {
		return nullProof( phone);
	}

	public String getPriority () {
		return nullProof( priority);
	}

	public String getProfileFullName () {
		return nullProof( profileFullName);
	}

	public String getRecId () {
		return nullProof( recId);
	}

	public String getResolutionString () {
		return nullProof( resolutionString);
	}

	public String getStatus () {
		return nullProof( status);
	}

	public String getService () {
		return nullProof( service);
	}

	public String getSource () {
		return nullProof( source);
	}

	public String getSubject () {
		return nullProof( subject);
	}

	public String getSymptom () {
		return nullProof( symptom);
	}

	public String getUrgency () {
		return nullProof( urgency);
	}

	public String getOwner () {
		return nullProof( owner);
	}

	public String getOwnerTeam () {
		return nullProof( ownerTeam);
	}

	public String getResolvedBy () {
		return nullProof( resolvedBy);
	}

	public String getSocialTextHeader () {
		return nullProof( socialTextHeader);
	}

	public String getPbc_Resubmission () {
		return nullProof( pbc_Resubmission);
	}

	public String getPbc_NewsReceived () {
		return nullProof( pbc_NewsReceived);
	}

	public String getPbc_Effort () {
		return nullProof( pbc_Effort);
	}

	public String getPbc_ServiceReqRecIDTemp () {
		return nullProof( pbc_ServiceReqRecIDTemp);
	}

	public String getPbc_JiraDescription () {
		return nullProof( pbc_JiraDescription);
	}

	public String getPbc_JiraSynched () {
		return nullProof( pbc_JiraSynched);
	}

	private String nullProof(String value){
		if(value != null){
			return value;
		}else {
			return "";
		}
	}
}
