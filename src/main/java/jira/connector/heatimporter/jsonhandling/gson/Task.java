package jira.connector.heatimporter.jsonhandling.gson;

import com.google.gson.annotations.SerializedName;

public class Task {

	@SerializedName ("AssignmentID")
	private String assignmentID;

	@SerializedName ("AssignedBy")
	private String assignedBy;

	@SerializedName ("CreatedBy")
	private String createdBy;

	@SerializedName ("Details")
	private String details;

	@SerializedName ("EstimatedEffort")
	private String estimatedEffort;

	@SerializedName ("Owner")
	private String owner;

	@SerializedName ("OwnerTeam")
	private String ownerTeam;

	@SerializedName ("OwnerType")
	private String ownerType;

	@SerializedName ("ParentLink_Category")
	private String parentLink_Category;

	@SerializedName ("ParentLink_RecID")
	private String parentLink_RecID;

	@SerializedName ("ParentLink")
	private String parentLink;

	@SerializedName ("RecId")
	private String recId;

	@SerializedName ("Status")
	private String status;

	@SerializedName ("Subject")
	private String subject;

	@SerializedName ("TaskType")
	private String taskType;

	@SerializedName ("Priority")
	private String priority;

	@SerializedName ("ParentObjectDisplayID")
	private String parentObjectDisplayID;

	@SerializedName ("ActualEffort")
	private String actualEffort;

	@SerializedName ("EmailAddress")
	private String emailAddress;

	@SerializedName ("Phone")
	private String phone;

	@SerializedName ("ResolvedBy")
	private String resolvedBy;

	@SerializedName ("PlannedEffort")
	private String plannedEffort;

	@SerializedName ("Service")
	private String service;

	@SerializedName ("PBC_Resolution")
	private String pbc_Resolution;

	@SerializedName ("PBC_Resubmission")
	private String pbc_Resubmission;

	@SerializedName ("PBC_Completion")
	private String pbc_Completion;

	public String getAssignmentID () {
		return nullProof( assignmentID);
	}

	public String getAssignedBy () {
		return nullProof( assignedBy);
	}

	public String getCreatedBy () {
		return nullProof( createdBy);
	}

	public String getDetails () {
		return nullProof( details);
	}

	public String getEstimatedEffort () {
		return nullProof( estimatedEffort);
	}

	public String getOwner () {
		return nullProof( owner);
	}

	public String getOwnerTeam () {
		return nullProof( ownerTeam);
	}

	public String getOwnerType () {
		return nullProof( ownerType);
	}

	public String getParentLink_Category () {
		return nullProof( parentLink_Category);
	}

	public String getParentLink_RecID () {
		return nullProof( parentLink_RecID);
	}

	public String getParentLink () {
		return nullProof( parentLink);
	}

	public String getRecId () {
		return nullProof( recId);
	}

	public String getStatus () {
		return nullProof( status);
	}

	public String getSubject () {
		return nullProof( subject);
	}

	public String getTaskType () {
		return nullProof( taskType);
	}

	public String getPriority () {
		return nullProof( priority);
	}

	public String getParentObjectDisplayID () {
		return nullProof( parentObjectDisplayID);
	}

	public String getActualEffort () {
		return nullProof( actualEffort);
	}

	public String getEmailAddress () {
		return nullProof( emailAddress);
	}

	public String getPhone () {
		return nullProof( phone);
	}

	public String getResolvedBy () {
		return nullProof( resolvedBy);
	}

	public String getPlannedEffort () {
		return nullProof( plannedEffort);
	}

	public String getService () {
		return nullProof( service);
	}

	public String getPbc_Resolution () {
		return nullProof( pbc_Resolution);
	}

	public String getPbc_Resubmission () {
		return nullProof( pbc_Resubmission);
	}

	public String getPbc_Completion () {
		return nullProof( pbc_Completion);
	}

	private String nullProof (String value) {
		if (value != null) {
			return value;
		} else {
			return "";
		}
	}
}
