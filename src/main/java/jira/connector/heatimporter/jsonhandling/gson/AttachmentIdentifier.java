package jira.connector.heatimporter.jsonhandling.gson;

import com.google.gson.annotations.SerializedName;

public class AttachmentIdentifier {
	@SerializedName ("ATTACHNAME")
	private String attachname;

	@SerializedName ("ParentLink_RecID")
	private String parentLinkRecId;

	@SerializedName ("RecId")
	private String recId;

	public String getAttachname () {
		return nullProof(attachname);
	}

	public String getParentLinkRecId () {
		return nullProof(parentLinkRecId);
	}

	public String getRecId () {
		return nullProof(recId);
	}

	private String nullProof (String value) {
		if (value != null) {
			return value;
		} else {
			return "";
		}
	}
}
