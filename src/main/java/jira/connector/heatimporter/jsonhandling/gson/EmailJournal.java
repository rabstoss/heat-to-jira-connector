package jira.connector.heatimporter.jsonhandling.gson;

import com.google.gson.annotations.SerializedName;

import jira.connector.heatimporter.cleanup.HTMLCleaner;

public class EmailJournal {

	@SerializedName ("SentOn")
	private String sentOn;

	@SerializedName ("FromAddr")
	private String fromAddr;

	@SerializedName ("ToAddrList")
	private String toAddrList;

	@SerializedName ("Subject")
	private String subject;

	@SerializedName ("EmailBody")
	private String emailBody;

	@SerializedName ("AttachmentName")
	private String attachmentName;

	@SerializedName ("AttachmentExtension")
	private String attachmentExtension;

	@SerializedName ("AttachmentData")
	private String attachmentData;

	@SerializedName ("RecId")
	private String recId;

	@SerializedName ("ParentLink_RecID")
	private String parentLink_RecID;

	public String getCommentBody () {
		return "Send on: " + nullProof(sentOn) + "\n" +
				"From: " + nullProof(fromAddr) + "\n" +
				"To: " + nullProof(toAddrList) + "\n" +
				"Subject: " + nullProof(subject) + "\n" +
				"Content: " + HTMLCleaner.clean(nullProof(emailBody));
	}

	public String getAttachmentName () {
		return nullProof(attachmentName);
	}

	public String getAttachmentExtension () {
		return nullProof(attachmentExtension);
	}

	public String getAttachmentData () {
		return nullProof(attachmentData);
	}

	public String getRecId () {
		return nullProof(recId);
	}

	public String getParentLink_RecID () {
		return nullProof(parentLink_RecID);
	}

	private String nullProof (String value) {
		if (value != null) {
			return value;
		} else {
			return "";
		}
	}
}
