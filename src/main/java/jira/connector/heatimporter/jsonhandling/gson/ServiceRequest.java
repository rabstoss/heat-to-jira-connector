package jira.connector.heatimporter.jsonhandling.gson;

import com.google.gson.annotations.SerializedName;

public class ServiceRequest {

	@SerializedName ("RecId")
	private String recId;

	@SerializedName ("CreatedBy")
	private String createdBy;

	@SerializedName ("Owner")
	private String owner;

	@SerializedName ("OwnerTeam")
	private String ownerTeam;

	@SerializedName ("ServiceReqNumber")
	private String serviceReqNumber;

	@SerializedName ("ClosedBy")
	private String closedBy;

	@SerializedName ("ResolvedBy")
	private String resolvedBy;

	@SerializedName ("OwnerEmail")
	private String ownerEmail;

	@SerializedName ("OwnerTeamEmail")
	private String ownerTeamEmail;

	@SerializedName ("OwnershipAssignmentEmail")
	private String ownershipAssignmentEmail;

	@SerializedName ("Email")
	private String email;

	@SerializedName ("ProfileFullName")
	private String profileFullName;

	@SerializedName ("Status")
	private String status;

	@SerializedName ("Service")
	private String service;

	@SerializedName ("Source")
	private String source;

	@SerializedName ("Urgency")
	private String urgency;

	@SerializedName ("Subject")
	private String subject;

	@SerializedName ("Symptom")
	private String symptom;

	@SerializedName ("IncidentLink_Category")
	private String incidentLink_Category;

	@SerializedName ("IncidentLink_RecID")
	private String incidentLink_RecID;

	@SerializedName ("IncidentLink")
	private String incidentLink;

	@SerializedName ("Phone")
	private String phone;

	@SerializedName ("Cost")
	private String cost;

	@SerializedName ("PBC_Resubmission")
	private String pbc_Resubmission;

	@SerializedName ("PBC_DateProduction")
	private String pbc_DateProduction;

	@SerializedName ("PBC_Completion")
	private String pbc_Completion;

	@SerializedName ("PBC_Effort")
	private String pbc_Effort;

	@SerializedName ("PBC_IncidentRecIDTemp")
	private String pbc_IncidentRecIDTemp;

	@SerializedName ("PBC_Developer_Valid")
	private String pbc_Developer_Valid;

	@SerializedName ("PBC_Developer")
	private String pbc_Developer;

	@SerializedName ("PBC_JiraDescription")
	private String pbc_JiraDescription;

	public String getRecId () {
		return nullProof( recId);
	}

	public String getCreatedBy () {
		return nullProof( createdBy);
	}

	public String getOwner () {
		return nullProof( owner);
	}

	public String getOwnerTeam () {
		return nullProof( ownerTeam);
	}

	public String getServiceReqNumber () {
		return nullProof( serviceReqNumber);
	}

	public String getClosedBy () {
		return nullProof( closedBy);
	}

	public String getResolvedBy () {
		return nullProof( resolvedBy);
	}

	public String getOwnerEmail () {
		return nullProof( ownerEmail);
	}

	public String getOwnerTeamEmail () {
		return nullProof( ownerTeamEmail);
	}

	public String getOwnershipAssignmentEmail () {
		return nullProof( ownershipAssignmentEmail);
	}

	public String getEmail () {
		return nullProof( email);
	}

	public String getProfileFullName () {
		return nullProof( profileFullName);
	}

	public String getStatus () {
		return nullProof( status);
	}

	public String getService () {
		return nullProof( service);
	}

	public String getSource () {
		return nullProof( source);
	}

	public String getUrgency () {
		return nullProof( urgency);
	}

	public String getSubject () {
		return nullProof( subject);
	}

	public String getSymptom () {
		return nullProof( symptom);
	}

	public String getIncidentLink_Category () {
		return nullProof( incidentLink_Category);
	}

	public String getIncidentLink_RecID () {
		return nullProof( incidentLink_RecID);
	}

	public String getIncidentLink () {
		return nullProof( incidentLink);
	}

	public String getPhone () {
		return nullProof( phone);
	}

	public String getCost () {
		return nullProof( cost);
	}

	public String getPbc_Resubmission () {
		return nullProof( pbc_Resubmission);
	}

	public String getPbc_DateProduction () {
		return nullProof( pbc_DateProduction);
	}

	public String getPbc_Completion () {
		return nullProof( pbc_Completion);
	}

	public String getPbc_Effort () {
		return nullProof( pbc_Effort);
	}

	public String getPbc_IncidentRecIDTemp () {
		return nullProof( pbc_IncidentRecIDTemp);
	}

	public String getPbc_Developer_Valid () {
		return nullProof( pbc_Developer_Valid);
	}

	public String getPbc_Developer () {
		return nullProof( pbc_Developer);
	}

	public String getPbc_JiraDescription () {
		return nullProof( pbc_JiraDescription);
	}

	private String nullProof (String value) {
		if (value != null) {
			return value;
		} else {
			return "";
		}
	}
}
