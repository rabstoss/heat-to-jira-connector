package jira.connector.heatimporter.jsonhandling.gson;

import com.google.gson.annotations.SerializedName;

public class NoteJournal {

	@SerializedName ("DisplayText")
	private String displayText;

	@SerializedName ("NotesBody")
	private String notesBody;

	@SerializedName ("CreatedDateTime")
	private String createdDateTime;

	@SerializedName ("CreatedBy")
	private String createdBy;

	@SerializedName ("Category")
	private String category;

	@SerializedName ("Subject")
	private String subject;

	@SerializedName ("AttachmentName")
	private String attachmentName;

	@SerializedName ("AttachmentExtension")
	private String attachmentExtension;

	@SerializedName ("AttachmentData")
	private String attachmentData;

	@SerializedName ("RecId")
	private String recId;

	@SerializedName ("ParentLink_RecID")
	private String parentLink_RecID;

	public String getCommentBody () {
		String contentBody = "";
		if (nullProof(displayText).equals(nullProof(notesBody))) {
			contentBody = nullProof(displayText);
		} else {
			contentBody = nullProof(displayText) + "\n" + nullProof(notesBody);
		}

		return "Created on: " + nullProof(createdDateTime) + "\n" +
				"By: " + nullProof(createdBy) + "\n" +
				"Category: " + nullProof(category) + "\n" +
				"Subject: " + nullProof(subject) + "\n" +
				"Content: " + nullProof(contentBody);
	}

	public String getAttachmentName () {
		return nullProof(attachmentName);
	}

	public String getAttachmentExtension () {
		return nullProof(attachmentExtension);
	}

	public String getAttachmentData () {
		return nullProof(attachmentData);
	}

	public String getRecId () {
		return nullProof(recId);
	}

	public String getParentLink_RecID () {
		return nullProof(parentLink_RecID);
	}

	private String nullProof (String value) {
		if (value != null) {
			return value;
		} else {
			return "";
		}
	}
}
