package jira.connector.heatimporter.jsonhandling;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;

import jira.connector.heatimporter.jsonhandling.gson.AttachmentIdentifier;
import jira.connector.heatimporter.jsonhandling.gson.EmailJournal;
import jira.connector.heatimporter.jsonhandling.gson.Incident;
import jira.connector.heatimporter.jsonhandling.gson.NoteJournal;
import jira.connector.heatimporter.jsonhandling.gson.ServiceRequest;
import jira.connector.heatimporter.jsonhandling.gson.Task;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class JsonHandler {

	public static List<Incident> translateStringToIncident(String request){
		JSONArray jsonArray = stringToJsonArray(request);
		List<Incident> incidents = new ArrayList<>();
		Gson gson = new Gson();

		for (int i = 0; i != jsonArray.length(); i++) {
			try {
				incidents.add(gson.fromJson(jsonArray.get(i).toString(), Incident.class));
			} catch (JSONException e) {
				PrintHandler.printGenericJSONConversionError(e);
			}
		}
		return incidents;
	}

	public static List<ServiceRequest> translateStringToServiceRequest(String request){
		JSONArray jsonArray = stringToJsonArray(request);
		List<ServiceRequest> serviceRequests = new ArrayList<>();
		Gson gson = new Gson();

		for (int i = 0; i != jsonArray.length(); i++) {
			try {
				serviceRequests.add(gson.fromJson(jsonArray.get(i).toString(),ServiceRequest.class));
			} catch (JSONException e) {
				PrintHandler.printGenericJSONConversionError(e);
			}
		}
		return serviceRequests;
	}

	public static List<Task> translateStringToTask(String request){
		JSONArray jsonArray = stringToJsonArray(request);
		List<Task>  tasks = new ArrayList<>();
		Gson gson = new Gson();

		for (int i = 0; i != jsonArray.length(); i++) {
			try {
				tasks.add(gson.fromJson(jsonArray.get(i).toString(), Task.class));
			} catch (JSONException e) {
				PrintHandler.printGenericJSONConversionError(e);
			}
		}
		return tasks;
	}

	public static List<EmailJournal> translateStringToEmailJournal(String request){
		JSONArray jsonArray = stringToJsonArray(request);
		List<EmailJournal> emailJournals = new ArrayList<>();
		Gson gson = new Gson();

		for (int i = 0; i != jsonArray.length(); i++) {
			try {
				emailJournals.add(gson.fromJson(jsonArray.get(i).toString(), EmailJournal.class));
			} catch (JSONException e) {
				PrintHandler.printGenericJSONConversionError(e);
			}
		}
		return emailJournals;
	}

	public static List<NoteJournal> translateStringToNoteJournal(String request){
		JSONArray jsonArray = stringToJsonArray(request);
		List<NoteJournal>  noteJournals = new ArrayList<>();
		Gson gson = new Gson();

		for (int i = 0; i != jsonArray.length(); i++) {
			try {
				noteJournals.add(gson.fromJson(jsonArray.get(i).toString(), NoteJournal.class));
			} catch (JSONException e) {
				PrintHandler.printGenericJSONConversionError(e);
			}
		}
		return noteJournals;
	}

	public static List<AttachmentIdentifier> translateStringToAttachmentIdentifier (String request){
		JSONArray jsonArray = stringToJsonArray(request);
		List<AttachmentIdentifier>  attachmentIdentifiers = new ArrayList<>();
		Gson gson = new Gson();

		for (int i = 0; i != jsonArray.length(); i++) {
			try {
				attachmentIdentifiers.add(gson.fromJson(jsonArray.get(i).toString(), AttachmentIdentifier.class));
			} catch (JSONException e) {
				PrintHandler.printGenericJSONConversionError(e);
			}
		}
		return attachmentIdentifiers;
	}

	private static JSONArray stringToJsonArray (String jsonString) {

		try {
			JSONObject s = new JSONObject(jsonString);
			return s.getJSONArray("value");
		} catch (Exception e) {
			PrintHandler.printGenericJSONConversionError(e);
			return new JSONArray();
		}
	}

	private JsonHandler(){}
}
