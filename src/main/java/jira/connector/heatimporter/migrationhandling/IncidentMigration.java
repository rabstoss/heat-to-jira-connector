package jira.connector.heatimporter.migrationhandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jira.connector.heatimporter.connection.ConnectionManager;
import jira.connector.heatimporter.jira.issueinput.IssueInputContainer;
import jira.connector.heatimporter.jira.issueinput.IssueInputFactory;
import jira.connector.heatimporter.jsonhandling.JsonHandler;
import jira.connector.heatimporter.jsonhandling.gson.Incident;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class IncidentMigration {

	private ConnectionManager cm;

	public IncidentMigration (ConnectionManager cm) {
		this.cm = cm;
	}

	public Map<String, String> startMigration () {
		try {
			return migrationOfIncidents();
		} catch (Exception e) {
			PrintHandler.printPartMigrationError("Incident", e);
			return new HashMap<>();
		}
	}

	private Map<String, String> migrationOfIncidents () {

		HashMap<String, String> tempRecIdMapping = new HashMap<>();
		int maxIncidents = cm.getHeatConnectionHandler().requestCountOfIncidents();
		boolean lastCall = false;
		int skip = 0;

		while (!lastCall) {
			List<Incident> tempList = new ArrayList<>();

			tempList.addAll(
					JsonHandler.translateStringToIncident(cm.getHeatConnectionHandler().requestIncidentsFromHeat(skip)));
			skip = skip + 100;
			if (skip > maxIncidents) {
				int top = 100 - (skip - maxIncidents);
				skip = skip - 100;
				tempList.addAll(
						JsonHandler.translateStringToIncident(
								cm.getHeatConnectionHandler().requestIncidentsFromHeat(skip, top)));
				lastCall = true;
			} else if (skip == maxIncidents) {
				lastCall = true;
			}
			tempRecIdMapping.putAll(cm
					.postIssuesToJira(convertIncidentToIssue(tempList)));
			if (skip % 10000 == 0) {
				PrintHandler.printMigrationTimeStamp("Incident", skip, maxIncidents);
			}
		}
		return tempRecIdMapping;
	}

	private List<IssueInputContainer> convertIncidentToIssue (List<Incident> incidents) {
		List<IssueInputContainer> issues = new ArrayList<>();
		for (int i = 0; i != incidents.size(); i++) {
			if (incidents.get(i).getPbc_JiraSynched().equals("true")) {
				issues.add(IssueInputFactory.build(incidents.get(i)));
			}
		}
		return issues;
	}
}