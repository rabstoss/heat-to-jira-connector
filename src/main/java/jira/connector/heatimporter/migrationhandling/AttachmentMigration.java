package jira.connector.heatimporter.migrationhandling;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jira.connector.heatimporter.connection.ConnectionManager;
import jira.connector.heatimporter.jsonhandling.JsonHandler;
import jira.connector.heatimporter.jsonhandling.gson.AttachmentIdentifier;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class AttachmentMigration {

	private ConnectionManager cm;

	public AttachmentMigration (ConnectionManager cm) {
		this.cm = cm;
	}

	public void startMigration (Map<String, String> recIdToJiraMapping) {

		try {
			migrateAllAttachmentsFromHeat(recIdToJiraMapping);
		} catch (Exception e) {

		}

	}

	private void migrateAllAttachmentsFromHeat (Map<String, String> recIdToJiraMapping) {

		int max = cm.getHeatConnectionHandler().requestCountOfAttachments();

		boolean lastCall = false;
		int skip = 0;
		while (!lastCall) {
			List<AttachmentIdentifier> tempList = new ArrayList<>();
			tempList.addAll(JsonHandler
					.translateStringToAttachmentIdentifier(
							cm.getHeatConnectionHandler().requestAttachmentsFromHeat(skip)));
			skip = skip + 100;
			if (skip > max) {
				int top = 100 - (skip - max);
				skip = skip - 100;
				tempList.addAll(JsonHandler.translateStringToAttachmentIdentifier(
						cm.getHeatConnectionHandler().requestAttachmentsFromHeat(skip, top)));
				lastCall = true;
			} else if (skip == max) {
				lastCall = true;
			}
			cm.postAttachmentToJira(tempList, recIdToJiraMapping);
			if (skip % 10000 == 0) {
				PrintHandler.printMigrationTimeStamp("Attachment", skip, max);
			}
		}
	}
}