package jira.connector.heatimporter.migrationhandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jira.connector.heatimporter.connection.ConnectionManager;
import jira.connector.heatimporter.jira.issueinput.IssueInputContainer;
import jira.connector.heatimporter.jira.issueinput.IssueInputFactory;
import jira.connector.heatimporter.jsonhandling.JsonHandler;
import jira.connector.heatimporter.jsonhandling.gson.ServiceRequest;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class ServiceRequestMigration {

	ConnectionManager cm;

	public ServiceRequestMigration (ConnectionManager cm) {
		this.cm = cm;
	}

	public HashMap<String, String> startMigration () {
		try {
			return migrationOfServiceRequests();
		} catch (Exception e) {
			PrintHandler.printPartMigrationError("ServiceRequest", e);
			return new HashMap<String, String>();
		}
	}

	private HashMap<String, String> migrationOfServiceRequests () {
		HashMap<String, String> tempRecIDMapping = new HashMap<>();
		int max = cm.getHeatConnectionHandler().requestCountOfServiceRequest();
		boolean lastCall = false;
		int skip = 0;

		while (lastCall == false) {
			List<ServiceRequest> tempList = new ArrayList<>();
			tempList.addAll(
					JsonHandler.translateStringToServiceRequest(cm.getHeatConnectionHandler().requestServiceRequestsFromHeat(skip)));
			skip = skip + 100;
			if (skip > max) {
				int top = 100 - (skip - max);
				skip = skip - 100;
				tempList.addAll(JsonHandler
						.translateStringToServiceRequest(
								cm.getHeatConnectionHandler().requestServiceRequestsFromHeat(skip, top)));
				lastCall = true;
			} else if (skip == max) {
				lastCall = true;
			}
			tempRecIDMapping.putAll(cm.postIssuesToJira(convertServiceRequestToIssue(tempList)));
			if(skip%10000 == 0) {
				PrintHandler.printMigrationTimeStamp("Service Request", skip, max);
			}
		}
		return tempRecIDMapping;
	}

	private static List<IssueInputContainer> convertServiceRequestToIssue (
			List<ServiceRequest> serviceRequestObjects) {
		List<IssueInputContainer> issues = new ArrayList<>();
		for (int i = 0; i != serviceRequestObjects.size(); i++) {
			issues.add(IssueInputFactory.build(serviceRequestObjects.get(i)));
		}
		return issues;
	}

}