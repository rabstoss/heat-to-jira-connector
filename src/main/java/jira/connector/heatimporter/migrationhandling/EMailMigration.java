package jira.connector.heatimporter.migrationhandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jira.connector.heatimporter.connection.ConnectionManager;
import jira.connector.heatimporter.jsonhandling.JsonHandler;
import jira.connector.heatimporter.jsonhandling.gson.EmailJournal;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class EMailMigration {

	ConnectionManager cm;

	public EMailMigration (ConnectionManager cm) {
		this.cm = cm;
	}

	public HashMap<String, String> startMigration (HashMap<String, String> recIdToJiraMapping) {
		HashMap<String, String> tempRecIdToJiraMapping = new HashMap<>();
		try {
			tempRecIdToJiraMapping.putAll(migrateAllEmailJournalsFromHeat(recIdToJiraMapping));
		} catch (Exception e) {
			PrintHandler.printPartMigrationError("Task", e);
		}
		return tempRecIdToJiraMapping;
	}

	private HashMap<String, String> migrateAllEmailJournalsFromHeat (HashMap<String, String> recIdToJiraMapping) {
		HashMap<String, String> tempRecIdToJiraMapping = new HashMap<>();
		int max = cm.getHeatConnectionHandler().requestCountOfJournalEmails();
		boolean lastCall = false;
		int skip = 0;
		while (lastCall == false) {
			List<EmailJournal> tempList = new ArrayList<>();
			tempList.addAll(
					JsonHandler
							.translateStringToEmailJournal(
									cm.getHeatConnectionHandler().requestJournalEmailsFromHeat(skip)));
			skip = skip + 100;
			if (skip > max) {
				int top = 100 - (skip - max);
				skip = skip - 100;
				tempList.addAll(JsonHandler
						.translateStringToEmailJournal(
								cm.getHeatConnectionHandler().requestJournalEmailsFromHeat(skip, top)));
				lastCall = true;
			} else if (skip == max) {
				lastCall = true;
			}
			tempRecIdToJiraMapping.putAll(cm.postEMailJournalsToJira(tempList,
					recIdToJiraMapping));
			if (skip % 10000 == 0) {
				PrintHandler.printMigrationTimeStamp("EMail Journal", skip, max);
			}
		}
		return tempRecIdToJiraMapping;
	}

}
