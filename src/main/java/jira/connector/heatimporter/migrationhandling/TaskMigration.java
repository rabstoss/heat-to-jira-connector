package jira.connector.heatimporter.migrationhandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jira.connector.heatimporter.connection.ConnectionManager;
import jira.connector.heatimporter.jsonhandling.JsonHandler;
import jira.connector.heatimporter.jsonhandling.gson.Task;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class TaskMigration {

	private ConnectionManager cm;

	public TaskMigration (ConnectionManager cm) {
		this.cm = cm;
	}

	public Map<String, String> startMigration (Map<String, String> recIDMapping) {
		try {
			return migrationOfTasks(recIDMapping);
		} catch (Exception e) {
			PrintHandler.printPartMigrationError("Tasks", e);
			return recIDMapping;
		}
	}

	private Map<String, String> migrationOfTasks (Map<String, String> recIdMapping) {
		HashMap<String, String> tempRecIdMapping = new HashMap<>();
		int max = cm.getHeatConnectionHandler().requestCountOfTask();
		boolean lastCall = false;
		int skip = 0;

		while (!lastCall) {
			List<Task> tempList = new ArrayList<>();
			tempList.addAll(
					JsonHandler.translateStringToTask(cm.getHeatConnectionHandler().requestTasksFromHeat(skip)));
			skip = skip + 100;
			if (skip > max) {
				int top = 100 - (skip - max);
				skip = skip - 100;
				tempList.addAll(
						JsonHandler
								.translateStringToTask(cm.getHeatConnectionHandler().requestTasksFromHeat(skip, top)));
				lastCall = true;
			} else if (skip == max) {
				lastCall = true;
			}
			tempRecIdMapping.putAll(cm.postTaskToJira(tempList, recIdMapping));
			if (skip % 10000 == 0) {
				PrintHandler.printMigrationTimeStamp("Task", skip, max);
			}
		}
		return tempRecIdMapping;
	}
}