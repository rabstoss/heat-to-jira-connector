package jira.connector.heatimporter.migrationhandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jira.connector.heatimporter.connection.ConnectionManager;
import jira.connector.heatimporter.jsonhandling.JsonHandler;
import jira.connector.heatimporter.jsonhandling.gson.NoteJournal;
import jira.connector.heatimporter.printhandler.PrintHandler;

public class NoteMigration {

	ConnectionManager cm;

	public NoteMigration (ConnectionManager cm) {
		this.cm = cm;
	}

	public HashMap<String, String> startMigration (HashMap<String, String> recIdToJiraMapping) {
		HashMap<String, String> tempRecIdToJiraMapping = new HashMap<>();
		try {
			tempRecIdToJiraMapping.putAll(migrateAllNoteJournalsFromHeat(recIdToJiraMapping));
		} catch (Exception e) {
			PrintHandler.printPartMigrationError("Task", e);
		}
		return tempRecIdToJiraMapping;
	}

	private HashMap<String, String> migrateAllNoteJournalsFromHeat (HashMap<String, String> recIdToJiraMapping) {
		int max = cm.getHeatConnectionHandler().requestCountOfJournalNotes();
		HashMap<String, String> tempRecIdToJiraMapping = new HashMap<>();
		boolean lastCall = false;
		int skip = 0;
		while (lastCall == false) {
			List<NoteJournal> tempList = new ArrayList<>();
			tempList.addAll(
					JsonHandler
							.translateStringToNoteJournal(
									cm.getHeatConnectionHandler().requestJournalNotesFromHeat(skip)));
			skip = skip + 100;
			if (skip > max) {
				int top = 100 - (skip - max);
				skip = skip - 100;
				tempList.addAll(JsonHandler
						.translateStringToNoteJournal(
								cm.getHeatConnectionHandler().requestJournalNotesFromHeat(skip, top)));
				lastCall = true;
			} else if (skip == max) {
				lastCall = true;
			}
			tempRecIdToJiraMapping.putAll(cm.postNoteJournalsToJira(tempList,
					recIdToJiraMapping));
			if (skip % 10000 == 0) {
				PrintHandler.printMigrationTimeStamp("Note Journal", skip, max);
			}
		}
		return tempRecIdToJiraMapping;
	}

}
