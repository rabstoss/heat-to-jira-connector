package jira.connector.heatimporter.jira.issueinput;

import com.atlassian.jira.rest.client.api.domain.input.IssueInput;

public class IssueInputContainer {

	private IssueInput issueInput;
	private String recId;

	public IssueInputContainer(IssueInput issueInput, String recId){
		this.issueInput = issueInput;
		this.recId = recId;
	}

	public IssueInput getIssueInput () {
		return issueInput;
	}

	public String getRecId () {
		return recId;
	}
}
