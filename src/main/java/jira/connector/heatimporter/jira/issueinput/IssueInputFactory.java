package jira.connector.heatimporter.jira.issueinput;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;

import jira.connector.heatimporter.cleanup.HTMLCleaner;
import jira.connector.heatimporter.jira.FieldTranslationHandler;
import jira.connector.heatimporter.jsonhandling.gson.Incident;
import jira.connector.heatimporter.jsonhandling.gson.ServiceRequest;
import jira.connector.heatimporter.jsonhandling.gson.Task;

public class IssueInputFactory {

	private static final long STORYID = 10001;
	private static final long TASKID = 10002;
	private static final long SUBTASKID = 10003;
	private static final long BUGID = 10102;

	public static IssueInputContainer build (Incident incident) {

		String owner = FieldTranslationHandler.translateUserNameToId(incident.getOwner());
		String team = FieldTranslationHandler.translateTeamNameToTeamId(incident.getOwnerTeam());

		return new IssueInputContainer(new IssueInputBuilder("HI", BUGID)
				.setFieldValue("assignee",
						new ComplexIssueInputFieldValue(createFieldForIssue("accountId", owner)))
				.setFieldValue("customfield_10561",
						new ComplexIssueInputFieldValue(createFieldForIssue("id", team)))
				.setFieldValue("labels", Arrays.asList("Heat-Migration"))
				.setFieldValue("customfield_10538",
						"http://pil-heat-sm.piller.de/HEAT/Default.aspx?" +
								"Scope=ObjectWorkspace&CommandId=Search&" +
								"ObjectType=Incident%23&" +
								"CommandData=RecId,%3D,0," + incident.getRecId())
				.setFieldValue("customfield_10526", incident.getEmail())
				.setSummary(shortenSummaryIfNecessary(
						incident.getSubject() + " " + incident.getSocialTextHeader()))
				.setDescription(HTMLCleaner.clean(incident.getPbc_JiraDescription()))
				.build(), incident.getRecId());
	}

	public static IssueInputContainer build (ServiceRequest serviceRequestObject) {

		String owner = FieldTranslationHandler.translateUserNameToId(serviceRequestObject.getOwner());
		String team = FieldTranslationHandler.translateTeamNameToTeamId(serviceRequestObject.getOwnerTeam());

		return new IssueInputContainer(new IssueInputBuilder("HI", STORYID)
				.setFieldValue("assignee", new ComplexIssueInputFieldValue(
						createFieldForIssue("accountId", owner)))
				.setFieldValue("customfield_10561",
						new ComplexIssueInputFieldValue(createFieldForIssue("id", team)))
				.setFieldValue("labels", Arrays.asList("Heat-Migration"))
				.setFieldValue("customfield_10538",
						"http://pil-heat-sm.piller.de/HEAT/Default.aspx?" +
								"Scope=ObjectWorkspace&CommandId=Search&" +
								"ObjectType=ServiceReq%23&" +
								"CommandData=RecId,%3D,0," + serviceRequestObject.getRecId())
				.setFieldValue("customfield_10526", serviceRequestObject.getEmail())
				.setSummary(shortenSummaryIfNecessary(serviceRequestObject.getSubject()))
				.setDescription(HTMLCleaner.clean(serviceRequestObject.getPbc_JiraDescription()))
				.build(), serviceRequestObject.getRecId());
	}

	public static IssueInputContainer build (Task taskObject) {

		String owner = FieldTranslationHandler.translateUserNameToId(taskObject.getOwner());
		String team = FieldTranslationHandler.translateTeamNameToTeamId(taskObject.getOwnerTeam());

		return new IssueInputContainer(new IssueInputBuilder("HI", TASKID)
				.setFieldValue("assignee",
						new ComplexIssueInputFieldValue(createFieldForIssue("accountId", owner)))
				.setFieldValue("customfield_10561",
						new ComplexIssueInputFieldValue(createFieldForIssue("id", team)))
				.setFieldValue("labels", Arrays.asList("Heat-Migration"))
				.setFieldValue("customfield_10538",
						"http://pil-heat-sm.piller.de/HEAT/Default.aspx?" +
								"Scope=ObjectWorkspace&CommandId=Search&" +
								"ObjectType=Task%23&" +
								"CommandData=RecId,%3D,0," + taskObject.getRecId())
				.setFieldValue("customfield_10526", taskObject.getEmailAddress())
				.setSummary(shortenSummaryIfNecessary(taskObject.getSubject()))
				.setDescription(HTMLCleaner.clean(taskObject.getDetails()))
				.build(), taskObject.getRecId());
	}

	public static IssueInputContainer build (Task taskObject, String parentKey) {

		String owner = FieldTranslationHandler.translateUserNameToId(taskObject.getOwner());
		String team = FieldTranslationHandler.translateTeamNameToTeamId(taskObject.getOwnerTeam());

		return new IssueInputContainer(new IssueInputBuilder("HI", SUBTASKID)
				.setFieldValue("assignee",
						new ComplexIssueInputFieldValue(createFieldForIssue("accountId", owner)))
				.setFieldValue("customfield_10561",
						new ComplexIssueInputFieldValue(createFieldForIssue("id", team)))
				.setFieldValue("parent", new ComplexIssueInputFieldValue(createFieldForIssue("key", parentKey)))
				.setFieldValue("customfield_10538",
						"http://pil-heat-sm.piller.de/HEAT/Default.aspx?" +
								"Scope=ObjectWorkspace&CommandId=Search&" +
								"ObjectType=Task%23&" +
								"CommandData=RecId,%3D,0," + taskObject.getRecId())
				.setFieldValue("labels", Arrays.asList("Heat-Migration"))
				.setSummary(shortenSummaryIfNecessary(taskObject.getSubject()))
				.setDescription(HTMLCleaner.clean(taskObject.getDetails()))
				.build(), taskObject.getRecId());
	}

	private static Map<String, Object> createFieldForIssue (String id, String value) {
		Map<String, Object> newField = new HashMap<>();
		newField.put(id, value);
		return newField;
	}

	private static String shortenSummaryIfNecessary (String summary) {
		if (summary.length() > 255) {
			return summary.substring(0, 244);
		} else {
			return summary;
		}
	}
}
