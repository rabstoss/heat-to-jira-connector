package jira.connector.heatimporter.jira;

public class FieldTranslationHandler {

	public static String translateUserNameToId (String owner) {
		/*
		switch (owner) {
			case "neue":
				return "5d19d5a391943f0d2ac518d8";
			case "azabstoss":
				return "5d4d50f5a70af10ceed33eb6";
			case "P10100127":
				return "557058:ac58e432-c118-428e-a0ed-585b67ccb4ab";
			case "P10100459":
				return "5e848b7b5b08780c1c3e786c";
			case "vkoch":
				return "5d38522d2acc2d0c6219d67a";
			case "P10100208":
				return "5c8672f2ee0f2d70e23c4989";
			case "kniese":
				return "5d38521a9761bd0c48ee8c88";
			case "azblum":
				return "5d4d510aba09fa0c50c6298e";
			case "awenzel":
				return "5d38520930951f0c8bd67abc";
			case "schliebe":
				return "5d3851f57a115a0c402a2d4c";
			case "chrapo":
				return "5d4d57cd0fa6d40d14fc9309";
			TODO:Change ID from azabstoss to corresponding Jira-ID
			case "tweise":
				return "5d4d50f5a70af10ceed33eb6";
			case "P10100115":
				return "5d4d50f5a70af10ceed33eb6";
			case "wirsing":
				return "5d4d50f5a70af10ceed33eb6";
			case "P10100457":
				return "5d4d50f5a70af10ceed33eb6";
			default:
				return "";
		}
		*/
		return "5d4d50f5a70af10ceed33eb6";
	}

	public static String translateTeamNameToTeamId (String teamName) {
		switch (teamName) {
			/*case "BIT":
				return "";*/
			case "BIT - Infrastructure Services":
				return "10308";
			case "BIT - Application Services":
				return "10307";
			case "Service Desk":
				return "10306";
			default:
				return "";
		}
	}

	public static String translateHeatStatusToJira (String status) {
		switch (status) {
			case "Logged":
				return "Waiting for support";
			//case "Active":	return "In Arbeit";
			//case "Active":	return "In Progress";
			case "Active":
				return "To Do";
			case "Closed":
				return "Done";
			//case "Active":	return "open";
			case "Resolved":
				return "Resolved";
			case "Fertig":
				return "Fertig";
			default:
				return "open";
		}
	}

	public static String translateTaskHeatStatusToJira (String status) {
		switch (status) {
			case "Completed":
				return "Fertig";
			default:
				return "open";
		}
	}

	public static String translateIncidentHeatProjectToJiraProject (String project) {
		switch (project) {
			case "Application Development Service":
				return "VEN";
			case "Application Development Projektierungs Tool":
				return "PROJ";
			case "APplus":
				return "APPLUS";
			case "Vent":
				return "VEN";
			case "Felios":
				return "PPS";
			case "Qlikview":
				return "CONT";
			case "SAP HR":
				return "HR";
			case "SAP C4C":
				return "CRM";
			case "Persis":
				return "HR";
			case "SAP Solutionmanager":
				return "SAP";
			case "Babtec":
				return "CAQ";
			case "CPWeb":
				return "FIBU";
			case "Felios-BDE":
				return "PPS";
			case "Varial":
				return "FIBU";
			case "Drigus":
				return "SAP";
			case "VentDim":
				return "VEN";
			case "SAP S/4":
				return "SAP";
			default:
				return "Allgemein";
		}
	}

	public static String translateServiceRequestHeatProjectToJiraProject (String project) {
		switch (project) {
			case "CPWeb":
				return "FIBU";
			case "Qlikview":
				return "CONT";
			case "Application Development Projektierungs Tool":
				return "PROJ";
			case "Vent":
				return "VEN";
			case "APplus":
				return "APPLUS";
			case "SAP C4C":
				return "CRM";
			case "Persis":
				return "HR";
			case "SAP HR":
				return "HR";
			case "Felios-BDE":
				return "PPS";
			case "VentDim":
				return "VEN";
			case "Babtec":
				return "CAQ";
			case "Felios":
				return "PPS";
			case "SAP S/4":
				return "SAP";
			case "SAP Solutionmanager":
				return "SAP";
			case "Application Development":
				return "VEN";
			case "Varial":
				return "FIBU";
			default:
				return "Allgemein";
		}
	}

	public static String translateTaskHeatProjectToJiraProject (String project) {
		switch (project) {
			case "SAP S/4":
				return "SAP";
			default:
				return "Allgemein";
		}
	}

	public static String translateHeatPriorityToJiraPriority (String project) {
		switch (project) {
			//case "High": return "Highest";
			case "High":
				return "High";
			case "Medium":
				return "Medium";
			case "Low":
				return "Low";
			default:
				return "IDEA";
		}
	}
}